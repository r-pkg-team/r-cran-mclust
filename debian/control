Source: r-cran-mclust
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-mclust
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-mclust.git
Homepage: https://cran.r-project.org/package=mclust
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev
Testsuite: autopkgtest-pkg-r

Package: r-cran-mclust
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: Gaussian Mixture Modelling for Model-Based Clustering
 This GNU R package supports Gaussian Mixture Modelling for Model-Based
 Clustering, Classification, and Density Estimation.
 .
 Gaussian finite mixture models fitted via EM algorithm for model-based
 clustering, classification, and density estimation, including Bayesian
 regularization, dimension reduction for visualisation, and
 resampling-based inference.
